
/// <reference path="Sdk.Soap.d.ts"/>

function test()
{
	var query = new Sdk.Query.QueryByAttribute( 'Account' );
	query.addAttributeValue( new Sdk.Guid( 'name' ) );
	Sdk.Q.retrieveMultiple( query ).then( entityCollection =>
	{
		alert( 'Result: ' + entityCollection );	
		
	}, error =>
	{
		alert( 'Error: ' + error );
	} );
}
