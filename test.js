/// <reference path="Sdk.Soap.d.ts"/>
function test() {
    var query = new Sdk.Query.QueryByAttribute('Account');
    query.addAttributeValue(new Sdk.Guid('name'));
    Sdk.Q.retrieveMultiple(query).then(function (entityCollection) {
        alert('Result: ' + entityCollection);
    }, function (error) {
        alert('Error: ' + error);
    });
}
//# sourceMappingURL=test.js.map